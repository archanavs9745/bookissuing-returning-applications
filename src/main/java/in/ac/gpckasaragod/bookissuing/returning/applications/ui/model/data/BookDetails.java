/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.bookissuing.returning.applications.ui.model.data;

/**
 *
 * @author student
 */
public class BookDetails {
 private Integer id;
 private String name;
 private String category;
 private Integer shelfno;
 private Integer timePeriod;
 private Integer fee;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getShelfno() {
        return shelfno;
    }

    public void setShelfno(Integer shelfno) {
        this.shelfno = shelfno;
    }

    public Integer getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(Integer timePeriod) {
        this.timePeriod = timePeriod;
    }

    public Integer getFee() {
        return fee;
    }

    public void setFee(Integer fee) {
        this.fee = fee;
    }

    public BookDetails(Integer id, String name, String category, Integer shelfno, Integer timePeriod, Integer fee) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.shelfno = shelfno;
        this.timePeriod = timePeriod;
        this.fee = fee;
    }
 
}
