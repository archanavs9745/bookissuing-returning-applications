/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.bookissuing.returning.applications.service;

import in.ac.gpckasaragod.bookissuing.returning.applications.ui.BookDetails;
import java.util.List;

/**
 *
 * @author student
 */
public interface BookService {
    public List<BookDetails>getAllBookDetails();
    public String updateBookDetails(Integer id,String name,String category,Integer shelfno,Integer timePeriod,Float fee );

    public String saveBookDetails(String name, String category, Integer shelfno, Integer timeperiod, Float fee);

    //public BookDetails readBookDetails(Integer id);

    public String deleteBookDetails(Integer id);

    //public List<BookDetails> getALLBookDetails();

    //public List<BookDetails> getALLBookDetails();

    public BookDetails readBookDetails(Integer id);

    public List<BookDetails> getALLBookDetails();

    
 
}
