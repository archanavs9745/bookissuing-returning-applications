/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.bookissuing.returning.applications.ui.model.data;

import java.sql.Date;

/**
 *
 * @author student
 */
public class ReturnDetails {
    private Integer id;
    private Date dateofissuing;
    private Float feereceived;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateofissuing() {
        return dateofissuing;
    }

    public void setDateofissuing(Date dateofissuing) {
        this.dateofissuing = dateofissuing;
    }

    public Float getFeereceived() {
        return feereceived;
    }

    public void setFeereceived(Float feereceived) {
        this.feereceived = feereceived;
    }
              
    
}
