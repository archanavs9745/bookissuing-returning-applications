/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.bookissuing.returning.applications.service;

import in.ac.gpckasaragod.bookissuing.returning.applications.ui.ReturnDetails;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author student
 */
public interface ReturnService {
    public String saveReturns(Date dateofissuing,Float feereceived );
    public ReturnDetails readReturns(Integer id);
    public List<ReturnDetails>getALLReturns();
    public String updateReturns(Date dateofissuing,Float feereceived);
    public String deleteReturns(Integer id);
    //public String updateReturns(Integer selectedId,Date dateofissuing,Integer feereceived);

   // public String saveReturns(String dateofissuing, Float feereceived);

    public String updateReturns(Integer selectedId, Date dateofissuing, Float feereceived);

    public String updateReturns(Integer selectedId, String dateofissuing, Float feereceived);

    public String saveReturns(String dateofissuing, Float feereceived);
    
}
