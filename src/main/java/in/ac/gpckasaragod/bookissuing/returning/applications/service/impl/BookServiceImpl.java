/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.bookissuing.returning.applications.service.impl;


import java.sql.Connection;
import java.sql.Statement;
import in.ac.gpckasaragod.bookissuing.returning.applications.service.BookService;
import in.ac.gpckasaragod.bookissuing.returning.applications.ui.BookDetails;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public  class BookServiceImpl extends ConnectionServiceImpl implements BookService{
    /**
     *
     * @return
     */

   @Override
    public List<BookDetails>getAllBookDetails(){
        List <BookDetails> books = new ArrayList<>();
       try{
         Connection connection = getConnection();
         Statement statement = connection.createStatement();
         String query = "SELECT * FROM BOOK ";
         ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){  
                int id = resultSet.getInt("ID");
                String bookname = resultSet.getString("BOOKNAME");
               String category = resultSet.getString("BOOK_CATEGORY");
               Integer shelfno = resultSet.getInt("SHELFNNO");
               Integer timeperiod = resultSet.getInt("TIMEPERIOD");
               Float fee = resultSet.getFloat("FEE_RECEIVED");
               BookDetails bookdetails = new BookDetails(id,bookname,category,shelfno,timeperiod,fee);
               bookdetails.add(bookdetails);
               
            }   
    }  catch (SQLException ex) {
           Logger.getLogger(BookServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
       }
    return books;
            }
  
    /**
     *
     * @param id
     * @param name
     * @param category
     * @param shelfno
     * @param timeperiod
     * @param fee
     * @return
     */
   @Override
    public String updateBookDetails(Integer id, String name, String category, Integer shelfno, Integer timeperiod,Float fee) {
       // throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    try{
      Connection connection = getConnection();
         Statement statement = connection.createStatement();
     
        /*String timeperiod = null;
        String bookname = null;
        String timeperiod = null;*/
         String query = "UPDATE BOOK SET BOOKNAME='"+name+"',BOOKCATEGORY'"+category+"',SHELFNO'"+shelfno+"',TIMEPERIOD'"+timeperiod+"',FEE'"+fee+"'WHERE ID="+id;  
          System.out.print(query);
          int update = statement.executeUpdate(query);
          if(update !=1)
              return "Udate failued";
          else
              return "Update successfully";
    }  catch (SQLException ex) {
           Logger.getLogger(BookServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
    }
       return null;
    
    }

    /**
     *
     * @param Id
     * @return
     */
    @Override
   public String deleteBookDetails(Integer Id){ //throws SQLException 
    try{
    Connection connection = getConnection();
     String query = "DELETE FROM BOOK WHERE ID =?";
     PreparedStatement statement = connection.prepareStatement(query);
     statement.setInt(1, Id);
     int delete = statement.executeUpdate();
     if (delete !=1)
         return "Delete failed";
     else
         return "Deleted successfully";
 }catch(SQLException ex){
     return "Delete failued";
 }
     
   }
    @Override
    public String saveBookDetails(String name, String category, Integer shelfno, Integer timeperiod, Float fee) {
      try{
    Connection connection = getConnection();

    Statement statement = connection.createStatement();
        
    String query ="INSERT INTO BOOK(BOOKNAME,BOOKCATEGORY,SHELFNO,TIMEPERIOD,FEE) VALUES" + "('"+name+ "','"+category+"','"+shelfno+"','"+timeperiod+"','"+fee+"')";
    System.err.println("Query:"+query);
    int status = statement.executeUpdate(query);
    if(status !=1) {
        return "save failed";
    }else {
        return "saved successfully";
}
  
   }catch (SQLException ex){
       Logger.getLogger(BookServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
       return "save failued";
   }  // throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public BookDetails readBookDetails() {
         BookDetails bookdetails = null  ;
    try{
        Connection connection = getConnection();
         Statement statement = connection.createStatement();
             String id = null;
           String query = "SELECT * FROM BOOK WHERE ID="+id;
           ResultSet resultSet = statement.executeQuery(query);
           while(resultSet.next()){
              // int id =resultSet.getInt("ID");
               String bookname = resultSet.getString("BOOKNAME");
               String category = resultSet.getString("BOOK_CATEGORY");
               Integer shelfno = resultSet.getInt("SHELFNNO");
               Integer timeperiod = resultSet.getInt("TIMEPERIOD");
               Float fee = resultSet.getFloat("FEE_RECEIVED");
               bookdetails = new BookDetails(id,bookname,category,shelfno,timeperiod,fee);
          
           }
    }  catch (SQLException ex) {
           Logger.getLogger(BookServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
       }
    
   return bookdetails;
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public BookDetails readBookDetails(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<BookDetails> getALLBookDetails() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

  

   
   

  
}   

    