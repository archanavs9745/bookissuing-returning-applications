/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.bookissuing.returning.applications.model.ui;

import java.util.Date;
/**
 *
 * @author student
 */
public class IssuingReturningUiModel {
    private Integer id;
    private String name;
    private String category;
    private Integer shelfno;
    private Integer timeperiod;
    private Float fee;
    private Date dateofissuing;
    private Integer feereceived;
    
  public IssuingReturningUiModel(Integer id,String name,String category,Integer shelfno,Integer timeperiod,Float fee,Date dateofissuing,Integer feereceived){
      this.id=id;
      this.name=name;
      this.category=category;
      this.shelfno=shelfno;
      this.timeperiod=timeperiod;
      this.fee=fee;
      this.dateofissuing=dateofissuing;
      this.feereceived=feereceived;
  }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getShelfno() {
        return shelfno;
    }

    public void setShelfno(Integer shelfno) {
        this.shelfno = shelfno;
    }

    public Integer getTimeperiod() {
        return timeperiod;
    }

    public void setTimeperiod(Integer timeperiod) {
        this.timeperiod = timeperiod;
    }

    public Float getFee() {
        return fee;
    }

    public void setFee(Float fee) {
        this.fee = fee;
    }

    public Date getDateofissuing() {
        return dateofissuing;
    }

    public void setDateofissuing(Date dateofissuing) {
        this.dateofissuing = dateofissuing;
    }

    public Integer getFeereceived() {
        return feereceived;
    }

    public void setFeereceived(Integer feereceived) {
        this.feereceived = feereceived;
    }
}
