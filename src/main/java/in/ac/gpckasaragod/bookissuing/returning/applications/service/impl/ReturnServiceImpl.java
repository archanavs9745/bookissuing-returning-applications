/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.bookissuing.returning.applications.service.impl;

import in.ac.gpckasaragod.bookissuing.returning.applications.service.ReturnService;
import in.ac.gpckasaragod.bookissuing.returning.applications.ui.ReturnDetails;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public abstract class ReturnServiceImpl extends ConnectionServiceImpl implements ReturnService{

    @Override
    public String saveReturns(Date dateofissuing, Float feereceived) {
        try{
            Connection connection = getConnection();
            Statement statement =connection.createStatement();
            String query = "INSERT INTO BOOK_ISSUING_RETURNING(DATE_OF_ISSUING,FEE_RECEIVED) VALUES" +"('"+dateofissuing+"','"+feereceived+"')";
            System.out.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status !=1 ){
                return "save failed";
            }else{
                return "save successfully";
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReturnServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            return "save failued";
        }
        
        
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }


    @Override
    public ReturnDetails readReturns(Integer id) {
        ReturnDetails returndetails = null;
        try{
            Connection connection = getConnection();
            Statement statement =connection.createStatement();
            String query = "SELECT * FROM BOOK_ISSUING_RETURNING WHERE ID="+id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
               Date dateofissuing = resultSet.getDate("DATE_OF_ISSUING");
               Integer feereceived = resultSet.getInt("FEE_RECEIVED");
               returndetails= new ReturnDetails(dateofissuing,feereceived);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReturnServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        return returndetails;
        
    }

    @Override
    public List<ReturnDetails> getALLReturns() {
        try{
            Connection connection = getConnection();
            Statement statement =connection.createStatement();
            String query = "SELECT * FROM BOOK_ISSUING_RETURNING";
            ResultSet resultSet =statement.executeQuery(query);
            while(resultSet.next()){
             int id =resultSet.getInt("ID");
              Date dateofissuing = resultSet.getDate("DATE_OF_ISSUING");
               Integer feereceived = resultSet.getInt("FEE_RECEIVED");
               ReturnDetails returndetails = new ReturnDetails(id,dateofissuing,feereceived);
               returndetails.add(returndetails);
            }
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }   catch (SQLException ex) {
            Logger.getLogger(ReturnServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String updateReturns(Date dateofissuing, Float feereceived) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query ="UPDATE BOOK_ISSUING_RETURNING SET DATE_OF_ISSUING'"+dateofissuing+"',FEE_RECEIVED'"+feereceived;
        System.out.print(query);
        int update = statement.executeUpdate(query);
        if(update!=1)
            return "Update failued";
        else
            return "Update successfully";
    }   catch (SQLException ex) {
            Logger.getLogger(ReturnServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String deleteReturns(Integer id) {
        try{
           Connection connection = getConnection(); 
           String query ="DELETE FROM BOOK_ISSUING_RETURNING WHERE ID=?";
           PreparedStatement statement = connection.prepareStatement(query);
           statement.setInt(1, id);
           int delete =statement.executeUpdate();
           if (delete !=1)
               return "DELETE FAILUED";
           else
               return "DELETE SUCCESSFULLY";
           
        } catch (SQLException ex) {
            Logger.getLogger(ReturnServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
       // throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        return null;
    }
}


   
   